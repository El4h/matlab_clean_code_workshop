# References and resources

## Books

- [The art of readable code](https://www.vitalsource.com/ie/products/the-art-of-readable-code-dustin-boswell-v9781449314217)
- [clean code](https://www.oreilly.com/library/view/clean-code-a/9780136083238/)

## Generic 

- [The Good Research Code Handbook](https://goodresearch.dev/)
- [OHBM Brainhack 2020 talk: #Matlab / #Octave : divide and conquer m-scripts](https://www.youtube.com/watch?v=AWfrlKTLkqw&list=PLVso6Qs8PLCiMMBXewYQjsAQLVtzAdJJX&index=4)


## Structuring your project

https://twitter.com/wmvanvliet/status/1240907591791886337

https://users.aalto.fi/~vanvlm1/papers/van_vliet_2020b.pdf

## Testing and refactoring

- [Test and code podcast](https://testandcode.com/)
    - [The Good Research Code Handbook - Patrick Mineault](https://testandcode.com/193)
    - [Testing in Scientific Research and Academia - Martin Héroux - part 1](https://testandcode.com/140)
    - [Testing in Scientific Research and Academia - Martin Héroux - part 2](https://testandcode.com/144)

- [Turing way: testing](https://the-turing-way.netlify.app/reproducible-research/testing.html)

- [Intro to refactoring](https://refactoring.guru/)

- [Catalog of reafactoring](https://refactoring.com/catalog/)


## Style guides

- [matlab-guidelines by nschloe](https://github.com/nschloe/matlab-guidelines)

- [matlab-guidelines by Richard Johnson](https://nl.mathworks.com/matlabcentral/fileexchange/46056-matlab-style-guidelines-2-0)


## Linter and formatter

- [Turing way: code quality](https://the-turing-way.netlify.app/reproducible-research/code-quality.html)
- [miss_hit](https://misshit.org/)


## Continuous integration

- [Turing way: continuous integration](https://the-turing-way.netlify.app/reproducible-research/ci.html)

- MATLAB in CI
    - [general documentation](https://github.com/mathworks)

- MATLAB in gitlab CI 
    - [documentation](https://github.com/mathworks/matlab-gitlab-ci-template)
    - [template pipeline](https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/lib/gitlab/ci/templates/MATLAB.gitlab-ci.yml)

## Templates

- [template repo for matlab project](https://github.com/Remi-Gau/template_matlab_analysis) 
    - [install with cookiecutter](https://github.com/Remi-Gau/template_matlab_analysis#install-with-cookiecutter)

<!-- ### Blog posts

- [Matlab clean code](https://remi-gau.github.io/2022/03/31/clean-matlab.html)
- [Transition your workflow](https://slides.com/djnavarro/workflow)
- [What I learned from porting a MatLab toolbox](https://www.adina-wagner.com/posts/portcode/_portmatlab/)
- [I Hate Matlab: How an IDE, a Language, and a Mentality Harm](https://neuroplausible.com/matlab) 
- [Matlab is a terrible programming language](https://www.rath.org/matlab-is-a-terrible-programming-language.html)

-->